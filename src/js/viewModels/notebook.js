/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your about ViewModel code goes here
 */
define(['knockout', 'ojs/ojbootstrap', 'ojs/ojresponsiveutils', 'ojs/ojresponsiveknockoututils',
        'ojs/ojmessaging', "jquery", "ojs/ojarraydataprovider",
        'ojs/ojknockout', 'ojs/ojinputtext', 'ojs/ojlabel', 'ojs/ojformlayout', 'ojs/ojmessages'],
    function (ko, Bootstrap, ResponsiveUtils, ResponsiveKnockoutUtils, Message, $, ArrayDataProvider,
    ) {

        function NotebookViewModel() {
            var self = this;
            var sessionId = Math.random().toString(36).substr(2, 9);

            // this.messages = [
            //     {
            //         severity: 'info',
            //         summary: "",
            //     }
            // ];

            this.result = "type your program and respect this format %<interpreter> <code>";
            self.result = ko.observable(this.result);

            // this.messagesDataprovider = new ArrayDataProvider(this.messages);
            // self.messagesDataprovider = ko.observable(this.messagesDataprovider);
            this.program = "";
            self.program = ko.observable(this.program);


            this.handleChange = function (event) {
                $.ajax({
                    url: "http://localhost:8080/execute",
                    type: 'post',
                    contentType: "application/json",
                    data: JSON.stringify({sessionId, code: self.program()}),
                    error: function (request, status, error) {
                        debugger;
                        self.result(request.responseJSON.message);
                    }
                }).done(function (data) {
                    self.result(data.result)
                    // TODO fix issue of messaging not rerendring
                    // self.messagesDataprovider(new ArrayDataProvider([
                    //     {
                    //         severity: 'info',
                    //         summary: data.result,
                    //     }
                    // ]));
                });
                return true;
            }.bind(this);


        }

        return new NotebookViewModel();
    }
);

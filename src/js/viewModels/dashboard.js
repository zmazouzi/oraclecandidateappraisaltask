/**
 * @license
 * Copyright (c) 2014, 2019, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define([
  "ojs/ojresponsiveutils",
  "ojs/ojresponsiveknockoututils",
  "knockout",
  "ojs/ojarraydataprovider",
  "jquery",
  "ojs/ojarraydatagriddatasource",
  "ojs/ojattributegrouphandler",
  "ojs/ojknockout",
  "ojs/ojchart",
  "ojs/ojselectcombobox",
  "ojs/ojinputtext",
  "ojs/ojlabel",
  "ojs/ojdatagrid",
  "ojs/ojlegend",
  "ojs/ojbutton"
], function(
  ResponsiveUtils,
  ResponsiveKnockoutUtils,
  ko,
  ArrayDataProvider,
  $,
  arrayModule,
  attributeGroupHandler
) {
  function DashboardViewModel() {
    var self = this;

    this.color1 = ko.observable('#267DB3');
    this.borderColor1 = ko.observable('#0F3248');
    this.markerShape1 = ko.observable('human');
    this.markerSize1 = ko.observable(20);

    this.plotAreaColor = ko.observable('#F2F2F2');
    this.plotAreaBorderColor = ko.observable('#000000');
    this.plotAreaBorderWidth = ko.observable(0);

    this.plotArea = ko.pureComputed(function() {
      return {
        backgroundColor: this.plotAreaColor(),
        borderColor: this.plotAreaBorderColor(),
        borderWidth: this.plotAreaBorderWidth()
      }
    }.bind(this));

    /* chart axes */
    this.xTitle = ko.observable('X-Axis Title');
    this.xStyle = ko.observable({"fontStyle":"italic","color":"#6070C7"});
    this.xMajorTickColor = ko.observable('#C4CED7');
    this.xMajorTickWidth = ko.observable(1);
    this.xMajorTickStyle = ko.observable('solid');
    this.xAxisLineColor = ko.observable('#9E9E9E');
    this.xAxisLineWidth = ko.observable(1);

    this.yTitle = ko.observable('Y-Axis Title');
    this.yStyle = ko.observable({"fontStyle":"italic","color":"#6070C7"});
    this.yAxisLineColor = ko.observable('#9E9E9E');
    this.yAxisLineWidth = ko.observable(1);
    this.yMajorTickColor = ko.observable('#C4CED7');
    this.yMajorTickWidth = ko.observable(1);
    this.yMajorTickStyle = ko.observable('solid');
    this.yTickLabelPosition = ko.observable('outside');

    this.xAxis = ko.pureComputed(function() {
      return {
        title: this.xTitle(),
        titleStyle: this.xStyle(),
        axisLine: {
          lineColor: this.xAxisLineColor(),
          lineWidth: this.xAxisLineWidth()
        },
        majorTick: {
          lineColor: this.xMajorTickColor(),
          lineWidth: this.xMajorTickWidth(),
          lineStyle: this.xMajorTickStyle()
        }
      };
    }.bind(this));

    this.yAxis = ko.pureComputed(function() {
      return {
        title: this.yTitle(),
        titleStyle: this.yStyle(),
        axisLine: {
          lineColor: this.yAxisLineColor(),
          lineWidth: this.yAxisLineWidth()
        },
        majorTick: {
          lineColor: this.yMajorTickColor(),
          lineWidth: this.yMajorTickWidth(),
          lineStyle: this.yMajorTickStyle()
        },
        tickLabel: {
          position: this.yTickLabelPosition()
        }
      };
    }.bind(this));

    let selectGraph = "";
    self.selectGraph = ko.observable(selectGraph);

    let inputValue =
      "0\tSeries 1\tCoke\t3\t28\t59\t8\t12\t17\n1\tSeries 1\tPepsi\t21\t65\t81\t24\t36\t44\n2\tSeries 1\tSnapple\t7\t49\t23\t16\t23\t32\n3\tSeries 1\tNestle\t8\t49\t92\t12\t16\t27\n4\tSeries 2\tCoke\t12\t35\t46\t17\t21\t24\n5\tSeries 2\tPepsi\t5\t47\t65\t14\t24\t31\n6\tSeries 2\tSnapple\t26\t71\t74\t37\t48\t52\n7\tSeries 2\tNestle\t10\t58\t36\t14\t37\t50\n";

    self.inputValue = ko.observable(inputValue);

    const parseInputString = input => {
      let matrix = input.split("\n").map(row => row.split("\t"));
      let rowLen = matrix[0].length;
      return matrix.filter(row => row.length === rowLen);
    };

    const getDistinctValues = matrix => {
     try {
         let distinct = [];
         for (let i = 0; i < matrix[0].length; i++) {
             let set = new Set();
             for (let j = 0; j < matrix.length; j++) {
                 set.add(matrix[j][i]);
             }
             let values = [...set].map(value =>
                 !isNaN(value) ? parseInt(value) : value
             );
             distinct.push({
                 index: i,
                 values,
                 size: set.size,
                 isNumber: !isNaN(values[0])
             });
         }
         distinct.sort((a, b) => {
             return a.size > b.size ? 1 : -1;
         });
         return distinct;
     } catch (e) {
         return [];
     }
    };

    const mapToScatterPlotFormat = matrix => {
      /**
       *  defines the number of distinct values for each column ( less distribution ) to use for series/groups
       *  in a scatter plot and the values that have the most distribution would be select for x/y/z values
       * @type {Array}
       */
      try {
        let distinct = getDistinctValues(matrix);
        const numericDistinctValues = distinct
          .filter(d => d.isNumber)
          .map(r => {
            return {
              ...r,
              min: Math.min(...r.values),
              max: Math.max(...r.values)
            };
          });
        this.message("Scatter plot render successful");
        return matrix.map(row => {
          const len = numericDistinctValues.length;
          return {
            group: row[distinct[0].index],
            series: row[distinct[1].index],
            z: parseInt(row[numericDistinctValues[len - 1].index]),
            y: parseInt(row[numericDistinctValues[len - 2].index]),
            x: parseInt(row[numericDistinctValues[len - 3].index])
          };
        });
      } catch (e) {
        self.message("Something went wrong, data is not visualizable  ");
        self.selectGraph("");
        return [];
      }
    };

    /**
     * we assume that the parsed payload will contain two  values qualitative
     * and 5 quantitative values for low/q1/q2/q3/high
     * @param matrix
     * @returns {*}
     */
    const mapToBoxPlotFormat = matrix => {
      try {
        /**
         *  defines the number of distinct values for each column ( less distribution ) to use for series/groups
         *  the rest of the values should be qualitative and the selection will follow the rule low < q1 < q2 < q3 < high
         * @type {Array}
         */
        let distinct = getDistinctValues(matrix);
        const numericDistinctValues = distinct.filter(d => d.isNumber);
        this.message("Box plot render successful");
        return matrix.map(row => {
          const len = numericDistinctValues.length;

          // min < q1 < q2 < q3 < max
          return {
            group: row[distinct[1].index],
            series: row[distinct[0].index],
            low: parseInt(row[numericDistinctValues[0].index]),
            q1: parseInt(row[numericDistinctValues[1].index]),
            q2: parseInt(row[numericDistinctValues[2].index]),
            q3: parseInt(row[numericDistinctValues[3].index]),
            high: parseInt(row[numericDistinctValues[4].index])
          };
        });
      } catch (e) {
        self.message("Something went wrong, data is not visualizable  ");
        self.selectGraph("");
        return [];
      }
    };


    const mapToPieChartFormat = matrix => {
      try {
        let distinct = getDistinctValues(matrix);
        const numericDistinctValues = distinct.filter(d => d.isNumber);
        this.message("Pie chart render successful");
        return matrix.map(row => {
          const len = numericDistinctValues.length;
          /**
           *  their should be at least one quantitative value for rendering groups we choose the value
           *  with the least distinct values groups/series and the one with most distinct values for the value attribute
           */
          return {
            group: row[distinct[0].index],
            series: row[distinct[1].index],
            value: parseInt(row[numericDistinctValues[len -1].index]),
          };
        });
      } catch (e) {
        self.message("Something went wrong, data is not visualizable  ");
        self.selectGraph("");
        return [];
      }
    };


    this.message = ko.observable("(No input was inserted YET)");

    // Scatter plot
    this.threeDValue = ko.observable("on");
    this.colorHandler = new attributeGroupHandler.ColorAttributeGroupHandler();
    this.shapeHandler = new attributeGroupHandler.ShapeAttributeGroupHandler();
    this.shapeHandler.getValue();
    this.scatterPlotDataProvider = new ArrayDataProvider([], {
      keyAttributes: "id"
    });
    self.scatterPlotDataProvider = ko.observable(this.scatterPlotDataProvider);

    // Box plot
    const $distinct = getDistinctValues(parseInputString(self.inputValue()));
    self.$distinct = ko.observable($distinct);

    this.boxPlotDataProvider = new ArrayDataProvider([], {
      keyAttributes: "id"
    });
    self.boxPlotDataProvider = ko.observable(this.boxPlotDataProvider);
    this.hiddenCategories = ko.observableArray([]);
    this.categoryInfo = ko.pureComputed(
      function() {
        var categories = this.hiddenCategories();
        return categories.length > 0 ? categories.join(", ") : "none";
      }.bind(this)
    );

    var legendSections = {
      sections: [
        {
          title: "Group",
          items: $distinct[1].values.map(v => {
            return {
              markerShape: this.shapeHandler.getValue(v),
              text: v,
              id: v
            };
          })
        },
        {
          title: "Series",
          items: $distinct[0].values.map(v => {
            return { color: this.colorHandler.getValue(v), text: v, id: v };
          })
        }
      ]
    };
    this.legendSections = ko.observable(legendSections);

    this.pieChartDataProvider = new ArrayDataProvider([], {
      keyAttributes: "id"
    });

    self.pieChartDataProvider = ko.observable(this.pieChartDataProvider);

    this.showVisualization = function(event) {
      if (event.target.id === "SCATTER_PLOT") {
        self.selectGraph("SCATTER_PLOT");
        self.scatterPlotDataProvider(
          new ArrayDataProvider(
            mapToScatterPlotFormat(parseInputString(this.inputValue())),
            { keyAttributes: "id" }
          )
        );
      } else if (event.target.id === "BOX_PLOT") {
        self.selectGraph("BOX_PLOT");
        self.boxPlotDataProvider(
          new ArrayDataProvider(
            mapToBoxPlotFormat(parseInputString(this.inputValue())),
            {
              keyAttributes: "id"
            }
          )
        );
      } else if (event.target.id === "PIE_CHART") {
        self.selectGraph("PIE_CHART");
        self.pieChartDataProvider(
            new ArrayDataProvider(mapToPieChartFormat(parseInputString(this.inputValue())), {
              keyAttributes: "id"
            })
        )
      }
      return true;
    }.bind(this);
  }

  return new DashboardViewModel();
});
